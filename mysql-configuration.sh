#!/usr/bin/env bash
### this should be used with root
## install sed if it isn't installed
yum install sed -y
## variables for sed
_cnf=`find /etc -name my.cnf -print`
_1='skip-character-set-client-handshake'
_2='collation-server=utf8_unicode_ci'
_3='character-set-server=utf8'
_5="default_time_zone='+00:00'"
## allow remote connections
_4='bind-address=127.0.0.1'
## running the first sed
sed -i "/\[mysqld\]/a$_1\n$_2\n$_3\n$_4\n$_5" "$_cnf"
## second sed for 'skip-networking' commented out
sed -i 's/skip-networking/# skip-networking/' "$_cnf"
